import json

"""Classe referente ao valor do token."""
class Text:
    def __init__(self, text):
        self.text = text

"""Classe referente ao grupo do token."""
class Group:
    def __init__(self, gruop):
        self.gruop = gruop

"""Classe referente a localização do token."""
class Locale:
    def __init__(self, index, line):
        self.index = index
        self.line = line

"""Classe referente ao token."""
class Token:
    def __init__(self, gruop, text):
        self.gruop = Group(gruop)
        self.text = Text(text)

    def setLocale(self, index, line):
        self.locale = Locale(index, line)

    def getFormmated(self):
        """Metodo utilizado para formatada o modelo do json."""
        return {"grupo": self.gruop.gruop, "texto": self.text.text, "local": {"linha": self.locale.line, "indice": self.locale.index}}

"""Classe referente ao token de erro."""
class ErrorToken:
    def __init__(self, text):
        self.text = Text(text)

    def setLocale(self, index, line):
        self.locale = Locale(index, line)

    def getFormmated(self):
        """Metodo utilizado para formatada o modelo do json."""
        return {"texto": self.text.text, "local": {"linha": self.locale.line, "indice": self.locale.index}}

"""Classe central para gerenciamento da criação do token."""
class Tokenizer:
    def __init__(self, program):
        self.program = program
        self.__resetAttributes()

    """Metodo utilizado para resetar os atributos utilizados durante o token central para gerenciamento da criação do token """
    def __resetAttributes(self):
        self.tokens = []
        self.erros = []
        self.pilha = []
        self.index_buffer_string = 0
        self.is_comentario = False
        self.line = 1
        self.index = 0
        self.if_operadores = False
        self.is_texto = False
        self.is_escape = False

    """Metodo responsavel pela converção do array para o json formatada."""
    def __toJson(self, array_token):
        return [elem.getFormmated() for elem in array_token]

    """Metodo responsavel pela remoção do espaço adicionado na identação dentro dos '{' '}' """
    def __removeIdentacao(self):
        if(len(self.pilha) > 1 and self.pilha[-2] == "{" and self.pilha[-1] == " "):
            self.pilha = self.pilha[:-1]
            self.index -= 1
    """Metodo responsavel pela validação se o caracter é um escape."""
    def __validedEscape(self, char):
        return char == "#" and not self.is_escape

    """Metodo responsavel pela validação de caracteres desconhecidos pela linguagem."""
    def __isValidedCaracteres(self, char):
        if(char in "@"):
            return self.__getDesconhecido(char)
        
    """Metodo responsavel pela validação se o caracter é um operadores de atribuição da linguagem."""
    def __isOperadores(self, char):
        return len(self.pilha) > 0 and self.pilha[-1] == ":" and char == ":"

    """Metodo responsavel pela validação se o caracter é um delimitador da linguagem."""
    def __isDelimitadores(self, char):
        return char in r"(){},"

    """Metodo responsavel pela validação se o caracter é um delimitador de texto da linguagem."""
    def __isDelimitadoresTexto(self, char):
        return char in r"'"

    """Metodo responsavel pela checagem se o operador é de atirbuição ou delimitador da linguagem."""
    def __isIfOperadores(self, char):
        return char == ":"

    """Metodo responsavel pela validação se o caracter é um operador matematico da linguagem."""
    def __isOperadoresMatematicos(self, char):
        return char in "<>+="

    """Metodo responsavel pela validação se o caracter é uma quebra de linha da linguagem."""
    def __isQuebraLinha(self, char):
        return char == r'\n'

    """Metodo responsavel pela validação se o caracter é um espaço em branco não utilizado por texto ou comentario."""
    def __isSpace(self, char):
        return char == " " and not self.is_comentario and not self.is_texto
    
    """Metodo responsavel pela validação se um inicio de comentario."""
    def __isComentario(self, char):
        return len(self.pilha) > 0 and self.pilha[-1] == '-' and char == '-'

    """Metodo responsavel pela criação do token de desconhecido."""
    def __getTokenDesconhecido(self, char):
        return Token("desconhecido", char)
    
    """Metodo responsavel pela criação do token de quebra de linha."""
    def __getQuebraLinha(self, char):
        return Token("quebra-linha", char)

    """Metodo responsavel pela criação dos tokens de atribuição."""
    def __getOperadores(self, char):
        if(char == ':'):
            return Token('atribuicao', '::')

    """Metodo responsavel pela criação dos tokens de operadores matematicos."""
    def __getOperadoresMatematicos(self, char):
        if(char == '<'):
            return Token('operador-menor', char)
        elif(char == '>'):
            return Token('operador-maior', char)
        elif(char == '+'):
            return Token('operador-mais', char)
        elif(char == '='):
            return Token('operador-igual', char)
        
    """Metodo responsavel pela sub-divisão entre as palvaras reservadas, numero e identificadores."""
    def __getTiposPalavra(self, text):
        if(text.isnumeric()):
            return Token('numero', text)
        elif(text == "Sim" or text == "Nao"):
            return Token('logico', text)
        elif(text[0].isupper() or text == "se" or text == "enquanto" or text == "retorna"):
            return Token("reservado", text)
        return Token("identificador", text)

    """Metodo responsavel pela criação do token de erro."""
    def __getDesconhecido(self, char):
        return ErrorToken("simbolo, {}, desconhecido".format(char))

    """Metodo responsavel pela possiveis uniones de mais mais de um token, dependedo da regra."""
    def __unionIdentificadores(self):
        new_tokens = []
        cont_pass = 0
        if(len(self.tokens) > 3):
            for index, _ in enumerate(self.tokens[:-1]):
                
                """ Validação utiliza uma comparaão se existe a ordem dos tokens SE, NAO e SE, se existir, 
                ele ira criar um novo utilizando o 'se nao se"""
                if((index+2) < len(self.tokens) and self.tokens[index].text.text == "se" and self.tokens[index+1].text.text == "nao"):
                    """Comparação utilizada na regra do 'SE NAO SE'."""
                    
                    if(self.tokens[index+2].text.text == "se"):
                        token_old = self.tokens[index]
                        token = Token("reservado", "se nao se")
                        token.locale = token_old.locale
                        new_tokens.append(token)
                        cont_pass += 2
                    else:
                        """Comparação utilizada na regra do 'SE NAO'."""
                        token_old = self.tokens[index]
                        token = Token("reservado", "se nao")
                        token.locale = token_old.locale
                        new_tokens.append(token)
                        cont_pass += 1
                        
                elif((index+2) < len(self.tokens) and self.tokens[index].text.text == "!" and self.tokens[index+1].text.text == "="):
                    """Comparação utilizada na regra do '!='."""
                    token_old = self.tokens[index]
                    token = Token("operador-diferente", "!=")
                    token.locale = token_old.locale
                    new_tokens.append(token)
                    cont_pass += 1
                    
                elif(cont_pass == 0):
                    new_tokens.append(self.tokens[index])
                    
                else:
                    cont_pass -= 1
                    
        return new_tokens

    """Metodo responsavel pela crição dos token delimitadores da linguagem."""
    def __getDelimitadores(self, char):
        if(char == '('):
            return Token("abre-parenteses", char)
        elif(char == ')'):
            return Token("fecha-parenteses", char)
        elif(char == ':'):
            return Token("dois-pontos", char)
        elif(char == '{'):
            return Token("abre-chaves", char)
        elif(char == '}'):
            return Token("fecha-chaves", char)
        elif(char == ','):
            return Token("virgula", char)

    """Metodo responsavel pela crição do token de texto."""
    def __getTexto(self):
        jail = self.pilha[self.index_buffer_string:]
        jail = "".join(jail)
        self.pilha = self.pilha[:-len(jail)]
        return Token("texto", jail)

    """Metodo responsavel pela crição do token de comentario."""
    def __getCadeiaCaracteresComentario(self):
        jail = []
        pilha_reverse = self.pilha[::-1]

        """ Metodo utiliza uma busca na pilha procurando o 
        caracter de inicio de comentario, a pilha necessitou ser invertida apenas para facilitar o desenvolvimento """
        for index, char in enumerate(pilha_reverse[:-1]):
            if(pilha_reverse[index] == "-" and pilha_reverse[index+1] == "-"):
                jail.append(pilha_reverse[index])
                jail.append(pilha_reverse[index+1])
                break
            else:
                jail.append(char)

        jail = "".join(jail[::-1])
        self.pilha = self.pilha[:-len(jail)]
        return Token("comentario", jail)

    """Metodo responsavel pela crição do token de cadeia de caracteres, sendo diferenciado 
    pelo retorno do metodo de Palvras."""
    def __getCadeiaCaracteres(self):
        jail = []

        """Metodo utiliza atraves da busca na pilha por caracteres delimitadores,
         espaco ou operadores de atribuição, para representar o inicio de um TEXTO"""
        for char in self.pilha[::-1]:

            if(self.__isDelimitadores(char) or char == " " or char == ":"):
                break
            else:
                jail.append(char)

        jail = "".join(jail[::-1])

        if(len(jail) == 0):
            return None
        else:
            self.pilha = self.pilha[:-len(jail)]
            return self.__getTiposPalavra(jail)

    """Metodo responsavel pela geração dos tipos de cadeias de caracteres 
    (Comentario | Texto | Identificadores | Reservados)."""
    def __generateCadeiaCaracteres(self):
        if(self.is_comentario):
            self.is_comentario = False
            token = self.__getCadeiaCaracteresComentario()
        else:
            token = self.__getCadeiaCaracteres()
        if(token is not None):
            token.setLocale(self.index-len(token.text.text), self.line)
            self.tokens.append(token)

    """Metodo responsavel pela geração dos delimitadores"""
    def __generateDelimitadores(self, char):
        token = self.__getDelimitadores(char)
        token.setLocale(self.index, self.line)
        self.tokens.append(token)

    """Metodo responsavel pela geração dos operadores de atribuição"""
    def __generateOperadores(self, char):
        token = self.__getOperadores(char)
        token.setLocale(self.index, self.line)
        self.tokens.append(token)
        self.pilha = self.pilha[:-1]

    """Metodo responsavel pela geração dos operadores matematicos"""
    def __generateOperadoresMatematicos(self, char):
        token = self.__getOperadoresMatematicos(char)
        token.setLocale(self.index, self.line)
        self.tokens.append(token)
        self.pilha = self.pilha[:-1]

    """Metodo responsavel pela geração da cadeia de caracteres que está seguida de um delimitador"""
    def __generateCadeiaCaracteresComDelimitadores(self, char):
        self.__generateCadeiaCaracteres()
        self.__generateDelimitadores(char)

    """Metodo responsavel pela geração da cadeia de caracteres que está seguida de um operador de atribuição"""
    def __generateCadeiaCaracteresComOperadores(self, char):
        self.__generateCadeiaCaracteres()
        self.__generateOperadores(char)

    """Metodo responsavel pela geração da cadeia de caracteres que está seguida de um operador matematico"""
    def __generateCadeiaCaracteresComOperadoresMatematicos(self, char):
        self.__generateCadeiaCaracteres()
        self.__generateOperadoresMatematicos(char)

    def __generateCadeiaCaracteresTexto(self, char):
        """Metodo responsavel pela geração da cadeia de carcteres"""
        token = self.__getTexto()
        if(token is not None):
            token.setLocale(self.index-len(str(token.text.text))+1, self.line)
            self.tokens.append(token)

    """Metodo responsavel pela geração da quebra de linhas"""
    def __generateQuebraLinha(self):
        
        self.__generateCadeiaCaracteres()
        token = self.__getQuebraLinha("\n")
        token.setLocale(self.index, self.line)
        self.tokens.append(token)
    
    """Metodo central responsavel pelo fluxo da Tokenizer"""
    def getTokens(self):
        

        self.__resetAttributes()

        for char_lines in self.program.splitlines():
            for char in char_lines:

                """Primeira Validação se caracter é valido"""
                erro_token = self.__isValidedCaracteres(char)
                if(erro_token is not None):
                    token = self.__getTokenDesconhecido(char)
                    token.setLocale(self.index, self.line)
                    self.tokens.append(token)
                    erro_token.setLocale(self.index, self.line)
                    self.erros.append(erro_token)
                else:

                    if(self.__validedEscape(char)):
                        """Segunda Validação se caracter é de escape"""
                        self.is_escape = True
                        self.pilha.append(char)
                    elif(self.is_escape):
                        self.is_escape = False
                        self.pilha.append(char)
                    else:

                        if(self.__isComentario(char)):
                            """Terceira Validação se caracter é de inicio de comentario"""
                            self.is_comentario = True

                        if(not self.is_comentario and not self.is_texto and self.__isDelimitadoresTexto(char)):
                            """Quarta Validação se caracter é de inicio de Texto"""
                            self.is_texto = True
                            self.index_buffer_string = len(self.pilha)
                            self.pilha.append(char)
                        elif(self.is_texto and self.__isDelimitadoresTexto(char)):
                            self.pilha.append(char)
                            self.__generateCadeiaCaracteresTexto(char)
                            self.is_texto = False
                        elif(self.is_texto):
                            self.pilha.append(char)
                        else:

                            if(self.__isIfOperadores(char)):
                                """Quinta Validação se caracter é um possivel operador de atribuição"""

                                if(self.if_operadores):
                                    self.if_operadores = False

                                    if(self.__isOperadores(char)):
                                        """Se for de operador de atribuição utilizar fluxo de ::"""
                                        pilha_antiga = self.pilha.pop()
                                        self.pilha = self.pilha[:-1]
                                        self.index -= 1
                                        self.__generateCadeiaCaracteresComOperadores(
                                            pilha_antiga)
                                        self.index += 1
                                    else:
                                        """Se não utilizar fluxo delimitadores"""
                                        pilha_antiga = self.pilha.pop()
                                        self.pilha = self.pilha[:-1]
                                        self.__generateCadeiaCaracteresComDelimitadores(
                                            pilha_antiga)

                                else:
                                    self.pilha.append(char)
                                    self.if_operadores = True

                            elif(self.if_operadores):
                                """Sexta Validação se caracter é um demilitador"""
                                pilha_antiga = self.pilha.pop()
                                self.pilha = self.pilha[:-1]
                                self.index -= 1
                                self.__generateCadeiaCaracteresComDelimitadores(
                                    pilha_antiga)
                                self.index += 1
                                self.if_operadores = False

                            """ Se caracter não se encaixar na validação que necessitam validar 
                            junto com a pilha, ele será validado novamente como um dos TIPOS abaixo """

                            if(self.__isDelimitadores(char)):
                                """Primeira Validação se caracter é um demilitador"""
                                self.__generateCadeiaCaracteresComDelimitadores(
                                    char)
                            elif(self.__isOperadoresMatematicos(char)):
                                """Segunda Validação se caracter é um operador matematico"""
                                self.__generateCadeiaCaracteresComOperadoresMatematicos(
                                    char)
                            elif(self.__isSpace(char)):
                                """Terceira Validação se caracter é um espaço não utilizado em Texto ou comentario"""
                                self.__generateCadeiaCaracteres()
                            else:
                                self.pilha.append(char)

                """Remove Identação dos '{' '}'"""
                self.__removeIdentacao()
                self.index += 1

            """Gerar quebra de linha """
            self.__generateQuebraLinha()

            self.line += 1
            self.index = 0

        self.tokens = self.__unionIdentificadores()
        return {"tokens": self.__toJson(self.tokens), "erros": self.__toJson(self.erros)}


def analisadorLexico(programa):
    tokenizer = Tokenizer(programa)
    return tokenizer.getTokens()
# ALERTA: Nao modificar o codigo fonte apos esse aviso


def compila(programa):
    # Caso o resultado nao seja igual ao teste
    # ambos sao mostrados e a execucao termina
    resultado = json.dumps(analisadorLexico(programa), indent=2)
    jsonresult = json.loads(resultado)
    with open("resultadoLexico.json", "w") as json_file:
        json.dump(jsonresult, json_file)
    if len(jsonresult["erros"]) > 0:
        print("Erro encontrado")
        print(jsonresult["erros"])
    else:
        print("\n Tokens gerados")


# Programa que é passado para a funcao analisadorLexico
programa = """-- funcao inicial

inicio:Funcao(valor:Logica,item:Texto):Numero::{
    logicoVar:Logico::Sim
    
    tiposDeVariaveis
    Logico::tiposDeFluxoDeControle
}

tiposDeVariaveis:Funcao::{
  textoVar:Texto::'#'exemplo##'
  numeroVar:Numero::1234
  logicoVar:Logico::Sim
}
tiposDeFluxoDeControle:Funcao:Logico::{
  resultado:Logico::Nao

  se(1 = 2){
    resultado::Nao
  } se nao se('a' != 'a'){
    resultado::Nao
  } se nao {
    resultado::Sim
  }

  contador:Numero::0
  enquanto(contador < 10){
    contador::contador + 1
  }

  retorna resultado
}"""
     
programa2 = """--funcao inicial

inicio:Funcao(){
	numeroVar:Valor1::60
	numeroVar:Valor2::100
	numeroVar:Valor3::120
	numeroVar:Maior::0	
    
	se(Valor1 > Valor2)
		Maior::Valor1
	se nao
		Maior::Valor2
	se(Valor1 > Valor3)
		Maior::Valor1
	se nao
		Maior::Valor3
	se(Valor2 > Valor3)
		Maior::Valor2
	se nao
		Maior::Valor3

	retorna Maior
	
}"""

# Execucao do teste que valida a funcao analisadorLexico
compila(programa2)
